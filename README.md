# Augmented WorkTime
###### _WorkTime reworked_

## Installation

Installing this script is super simple and can be done in two steps:

1. Install TamperMonkey extension from [Chrome Web Store](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo//Open)
2. Open latest release URL [here](https://gitlab.com/worktime-helper/augmented-worktime/raw/release/app/out/worktime.user.js) and click 'Install' button

Done!

## Updates

Updates are handled automatically by tampermonkey. In its settings you can configure how often you want to download updates. It is also possible to check for updates manually, either:

* click on tampermonkey icon and select check for updates
* open latest release URL and select install.

## Settings
![settings-window](.assets/settings-window.png)

Due to couple new features it is necessary to set some parameters. After first installation settings window will appear. You can access Settings at any time using F7 key.

### InFront specific settings
To toggle this section use 'InFront contractor toggle'. This lets AWT know that you work for InFront.

#### Contractor name
This value is used as name of generated log file. According to InFront regulations this name should be made of first letter of your name followed by whole surname

#### Log issue name
This value is used in generated xlsx file to fill LOG_ISSUE_NAME column. In few words describe what you do.

### UX / UI settings

#### Auto fill entry
With this setting AWT will fill log work description field with predefined text 'Implementation and tests'. In feature releases this text will be customizable (see [#39](https://gitlab.com/worktime-helper/augmented-worktime/issues/39)).

Also `Ctrl+Enter` submits log window.

#### Main Page Reworked
![main-page-reworked](.assets/main-page-reworked.png)

Enables refreshed view of the main tab.

### Time and finance

#### How many PLNs per hour
This **optional** value is used to calculate approximate income based on worked hours. Calculated data is presented on Monthly Work Chart, while hovering on any day. Leave it empty if you don't want AWT to present such data.

#### Monthly progress calculation method
This toggle switches how Monthly Progress Chart calculates its view. Two modes are available:

##### Day
With this setting selected, in next field provide number of hours you plan to work every day. MWP will set this value as # of hours you need to work every day, and calculate expected total hours at the end of the month. It takes into account non working days or holidays taken from [holid.co](holid.co)

##### Month
With this settings selected, in next fields fill how many hours you plan to work each month. There is a helper button that fills values for each month for you. It takes data from [kalendarzswiat.pl](www.kalendarzswiat.pl) to fetch expected hours per month.
If you want to work more (or less) you can modify automatically populated values. MWP will then recalculate how many hours you need to work each day to achieve desired goal.

## Month Work Progress chart
![month-work-progress](.assets/month-progress-chart.png)

Chart shows your month's work. Hovering over columns displays stats on that day. Hovering over certain elements explains them.

* Solid yellow line represents actual work done to this point
* Dashed yellow line shows how many hours you will work if you will be working specified amount of hours a day.
* Pink line represents required amount of hours. If your yellow line is above you worked overtime.
* Thin red lines mark required hours per day and month.
* Required hours are set in settings, either fixed, like 8h/day or calculated from monthly goal (ie. 200h/20days = 10h)

## Work Calendar
![work-calendar](.assets/work-calendar.png)

Popular calendar layout that shows how many hours you worked every day and total sums for each week.

## Shortcuts Legend
![shortcuts-legend](.assets/shortcuts-legend.jpg)

Hovering over small '?' sign in bottom right corner will open Legend with keyboard shortcuts.

## Extra timers
![extra-timers](.assets/extra-timers.png)

This timer shows when you can leave work. Time is based on break and hrs per day specified in settings.

## Work Entries Table
![wet](.assets/work-entries-table.png)

#### Extract work logs button

This feature takes worklog entries from the Work Entries Table and parses them to xlsx, in InFront format.

#### Fetch work logs button

This feature downloads logs directly from worktime endpoint and also parses them to xlsx. Might differ slightly from table values.
